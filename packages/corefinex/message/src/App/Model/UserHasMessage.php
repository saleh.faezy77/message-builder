<?php

namespace Corefinex\Message\App\Model;

use App\User;

class UserHasMessage extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user_has_message';
    public $timestamps = false;
    protected $fillable = [
        'message_id',
        'user_id',
        'seen_at'
    ];

    public function Message()
    {
        return $this->hasOne(Message::class, 'id', 'message_id');
    }

    public function User()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getHasSeenAttribute()
    {
        return $this->attributes['seen_at'] != null ?
            [
                'bg' => 'success',
                'text' => 'مشاهده شده'] :
            [
                'bg' => 'dark',
                'text' => 'مشاهده نشده'
            ];
    }

    public function getUrlAttribute()
    {
        return route('message.overview', ['id' => $this->attributes['id']]);
    }
}
