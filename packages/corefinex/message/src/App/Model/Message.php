<?php

namespace Corefinex\Message\App\Model;

use App\User;

class Message extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = [
        'sender_id', 'title', 'description', 'level', 'type'
    ];
    protected $table = 'messages';

    public function Receivers()
    {
        return $this->hasMany(UserHasMessage::class, 'message_id', 'id')->with('User');
    }

    public function Sender()
    {
        return $this->hasOne(User::class, 'id', 'sender_id');
    }

    public function getUrlAttribute()
    {
        return route('message.show', ['message' => $this->attributes['id']]);
    }

    public function getTypeTextAttribute()
    {
        switch ($this->attributes['type']) {
            case 'public':
                return [
                    'bg' => 'success',
                    'text' => 'عمومی(همه کاربران)'
                ];
                break;
            case 'private':
                return [
                    'bg' => 'info',
                    'text' => 'خصوصی'
                ];
                break;
            default:
                return [
                    'bg' => 'dark',
                    'text' => 'نامشخص'
                ];
                break;
        }
    }

    public function getLevelTextAttribute()
    {
        switch ($this->attributes['level']) {
            case 'low':
                return [
                    'bg' => 'info',
                    'text' => 'کم'
                ];
                break;
            case 'normal':
                return [
                    'bg' => 'success',
                    'text' => 'متوسط'
                ];
                break;
            case 'high':
                return [
                    'bg' => 'danger',
                    'text' => 'بالا'
                ];
                break;
            default:
                return [
                    'bg' => 'dark',
                    'text' => 'نامشخص'
                ];
                break;
        }
    }
}
