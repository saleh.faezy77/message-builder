<?php

namespace Corefinex\Message\App\Controller;

use App\User;
use Corefinex\Message\App\Model\Message;
use Corefinex\Message\App\Model\UserHasMessage;
use Corefinex\Message\MessageFacade;
use Illuminate\Http\Request;

class MessageCrud extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $messages = MessageFacade::paginate();
        return view('CorefinexMessageView::index', compact('messages'));
    }

    public function create()
    {
        return view('CorefinexMessageView::create');
    }

    public function store(Request $request)
    {
        $me = MessageFacade::create($request->title, $request->description, $request->level, $request->type);
        if ($me) {
            $users = User::query()
                ->when($request->type == 'private', function ($q) use ($request) {
                    $q->where('id', $request->users_list);
                })
                ->get();
            $sync = MessageFacade::sync_user($me, $users);
            return $sync ? redirect()->back()->withErrors(['message' => 'با موفقیت ارسال شد']) :
                redirect()->back()->withErrors(['message' => 'خظای رخ داد. مجدد تلاش کنید']);
        }
        return redirect()->back()->withErrors(['message' => 'خظای رخ داد. مجدد تلاش کنید']);
    }


    public function show(Message $message)
    {
        return view('CorefinexMessageView::overview', compact('message'));
    }

    public function edit(Message $message)
    {
        return view('CorefinexMessageView::create', compact('message'));
    }

    public function update(Message $message, Request $request)
    {

    }

    public function destroy(Message $message)
    {
        $uhm = UserHasMessage::query()->where('message_id', $message)->delete();
        $me = Message::query()->where('id', $message)->delete();
        return $uhm && $me ?
            redirect()->back()->withErrors(['message' => 'پیام مورد نظر با موفقیت حذف شد']) :
            redirect()->back()->withErrors(['message' => 'خطای در فرایند حذف پیام رخ داد'])->setStatusCode(500);
    }

}
