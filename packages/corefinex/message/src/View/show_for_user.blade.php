@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $message->message->title ?? '-' }}</div>
                    <div class="card-body">
                        {!! $message->message->description ?? '-' !!}
                    </div>
                    <div class="card-footer">
                        <span
                            class="text text-{{ $message->message->LevelText['bg'] ?? 'dark' }}">اهمیت :  {{ $message->message->LevelText['text'] ?? 'متوسط' }}</span>
                        - <span
                            class="badge bg-{{ $message->message->TypeText['bg'] ?? 'dark' }}">نوع پیام : {{ $message->message->TypeText['text'] ?? 'متوسط' }}</span>
                        - زمان ارسال <small>{{ jdate($message->message->created_at)->format('Y-m-d ساعت H:i') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
