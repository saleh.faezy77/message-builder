@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <h6 class="border-bottom pb-2 mb-0">
                        لیست پیام ها
                        <small style="float: left"><a href="{{ route('message.create') }}">ایجاد پیام جدید</a></small>
                    </h6>
                    @forelse($messages as $message)

                        <div class="d-flex text-muted pt-3 border-bottom">
                            <div class="pb-3 mb-0 small lh-sm  w-100">
                                <strong
                                    class="d-block text-gray-dark">{{ $message->title ?? 'عنوان' }}
                                    - <span
                                        class="text text-{{ $message->LevelText['bg'] ?? 'dark' }}">اهمیت :  {{ $message->LevelText['text'] ?? 'متوسط' }}</span>
                                    - <span
                                        class="badge bg-{{ $message->TypeText['bg'] ?? 'dark' }}">نوع پیام : {{ $message->TypeText['text'] ?? 'متوسط' }}</span>
                                    - زمان ارسال <small>{{ jdate($message->created_at)->format('Y-m-d ساعت H:i') }}</small>

                                </strong>
                                <br>
                                <p class="w-100">
                                    {!! $message->description !!}
                                </p>
                                <small>
                                    <a href="{{ $message->url }}">مشاهده کلی</a>
                                </small>
                            </div>
                        </div>
                    @empty
                        <div class="d-flex text-muted pt-3">
                            <div class="alert alert-info w-100">
                                تا کنون هیچ پیامی ارسال نشده است
                            </div>
                        </div>
                    @endforelse
                    <small class="d-block text-end mt-3">
                        {!! $messages->links() !!}
                    </small>
                </div>

            </div>
        </div>
    </div>
@endsection
