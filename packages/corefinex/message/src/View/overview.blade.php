@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $message->title ?? '-' }}</div>
                    <div class="card-body">
                        {!! $message->description ?? '-' !!}
                    </div>
                    <div class="card-footer">
                        <span
                            class="text text-{{ $message->LevelText['bg'] ?? 'dark' }}">اهمیت :  {{ $message->LevelText['text'] ?? 'متوسط' }}</span>
                        - <span
                            class="badge bg-{{ $message->TypeText['bg'] ?? 'dark' }}">نوع پیام : {{ $message->TypeText['text'] ?? 'متوسط' }}</span>
                        - زمان ارسال <small>{{ jdate($message->created_at)->format('Y-m-d ساعت H:i') }}</small>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-header">دریافت کنندگان</div>
                    <div class="card-body">
                        <table class="table table-light">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>نام</td>
                                <td>آدرس ایمیل</td>
                                <td>وضیعت مشاهده</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($message->Receivers as $us)
                                <tr>
                                    <td>{{ $us->id }}</td>
                                    <td>{{ $us->user->name }}</td>
                                    <td>{{ $us->user->email }}</td>
                                    <td>
                                        <span class="badge bg-{{ $us->HasSeen['bg'] }}">
                                            {{ $us->seen_at != null ? 'مشاهده شده : ' . jdate($us->seen_at)->format('Y-m-d ساعت H:i')  : $us->HasSeen['text'] }}
                                        </span>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
