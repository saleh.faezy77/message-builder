@extends('layouts.app')

@section('content')
    <style>
        span.select2.select2-container.select2-container--default.select2-container--below.select2-container--focus {
            width: 100% !important;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="my-3 p-3 bg-body rounded shadow-sm">
                    <h6 class="border-bottom pb-2 mb-0">
                        ایجاد پیام جدید
                        <small style="float: left"><a href="{{ route('message.index') }}">بازگشت به لیست پیام
                                ها</a></small>
                    </h6>
                    <form action="{{ route('message.store') }}" method="post">
                        @csrf
                        <div class="form-row">
                            <label for="title" class="label">عنوان</label>
                            <input class="form-control" type="text" name="title" id="title"
                                   placeholder="عنوان پیام را وارد کنید">
                        </div>
                        <div class="form-row">
                            <label for="description" class="label">متن پیام</label>
                            <textarea class="form-control" name="description" id="description"
                                      placeholder="عنوان پیام را وارد کنید"></textarea>
                        </div>
                        <div class="form-row">
                            <label for="type" class="label">نوع پیام</label>
                            <select id="type" name="type" class="form-control">
                                <option value="public">عمومی - تمامی کاربران</option>
                                <option value="private">خصوصی - کاربران خاص</option>
                            </select>
                        </div>
                        <div class="form-row">
                            <label for="level" class="label">اهمیت پیام</label>
                            <select id="level" name="level" class="form-control">
                                <option value="low">کم</option>
                                <option value="normal">متوسط</option>
                                <option value="high">بالا</option>
                            </select>
                        </div>
                        <div class="form-row">
                            <label for="send_type" class="label">ارسال به صورت</label>
                            <select id="send_type" name="send_type" class="form-control">
                                <option value="low">ایمیل</option>
                                <option value="normal">پیامک</option>
                            </select>
                        </div>
                        <div class="form-row" id="users_list">
                            <label for="users" class="label">کاربران دریافت کننده</label>
                            <select id="users" name="users_list" class="form-control"></select>
                        </div>
                        <div class="form-row mt-2">
                            <button type="submit" class="btn btn-block btn-success w-100">ارسال</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#description',
            menubar: false,
            directionality : "rtl"
        });
    </script>
    <script>
        $(document).ready(() => {
            $('#users_list').hide();
        })
        $('#type').on('change', () => {
            console.log()
            if ($('#type').val() === 'private') {
                $('#users').attr('name', 'users_list')
                return $('#users_list').show();
            }
            $('#users').attr('name', null)
            return $('#users_list').hide();
        })
        $('#users').select2({
            ajax: {
                url: '{{ route('users_for_select2') }}',
                dataType: 'json',
                type: "get",
                data: function (term) {
                    return {
                        term: term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            }
                        })
                    };
                }
            }
        });
    </script>
@endsection
