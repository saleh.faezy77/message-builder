<?php

use Illuminate\Support\Facades\Route;
require_once __DIR__.'/api.php';
Route::resource('message', \Corefinex\Message\App\Controller\MessageCrud::class)->middleware(['web', 'auth']);
Route::get('api/v1/users', function () {
    $users = \App\User::query()
        ->select(['id as id', 'name as name'])
        ->get();
    return response()->json($users, 200);
})->name('users_for_select2')->middleware(['web', 'auth']);;
Route::get('notify/{id}', function ($id) {
    $message = \Corefinex\Message\App\Model\UserHasMessage::query()->where('id', $id)
        ->where('user_id', user()->id)
        ->firstOrFail();
    if ($message && $message->seen_at != null){
        $message->seen_at = now();
        $message->save();
    }
    return view('CorefinexMessageView::show_for_user', compact('message'));
})->name('message.overview')->middleware(['web', 'auth']);;
Route::get('te',function (){
    dd(\Corefinex\Message\MessageFacade::delete());
});
