<?php

namespace Corefinex\Message\Interfaces;

interface MessageInterface
{
    public function Send($title, $body, $driver, $theme = null);

}
