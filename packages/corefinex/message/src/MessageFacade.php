<?php

namespace Corefinex\Message;

class MessageFacade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Message';
    }
}
