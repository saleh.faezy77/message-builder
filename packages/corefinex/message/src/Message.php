<?php

namespace Corefinex\Message;

use App\User;
use Corefinex\Message\App\Model\UserHasMessage;

class Message
{
    public function get()
    {
        return \Corefinex\Message\App\Model\Message::query()->get();
    }
    public function delete()
    {
        return \Corefinex\Message\App\Model\Message::query()->delete();
    }

    public function paginate()
    {
        return \Corefinex\Message\App\Model\Message::query()->paginate();
    }

    public function create($title, $description, $level = 'normal', $type = 'high')
    {
        $new = new \Corefinex\Message\App\Model\Message([
            'sender_id' => user()->id,
            'title' => $title ?? 'بدون عنوان',
            'description' => $description ?? 'بدون توضیحات',
            'level' => $level ?? 'normal',
            'type' => $type ?? 'public'
        ]);
        $new->save();
        return $new;
    }

    public function sync_user(\Corefinex\Message\App\Model\Message $message, $users)
    {
        try {
            foreach ($users as $user) {
                $insert = new UserHasMessage([
                    'message_id' => $message->id,
                    'user_id' => (int)$user->id,
                    'seen_at' => null
                ]);
                $insert->save();
            }
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
