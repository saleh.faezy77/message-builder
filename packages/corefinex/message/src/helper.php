<?php
if (!function_exists('user')) {
    function user($id = null)
    {
        if ($id != null) {
            return \App\User::query()->where('id', $id)->first() ?? null;
        }
        return auth()->check() ? auth()->user() : null;
    }
}

if (!function_exists('get_user_message')) {
    function get_user_message($message_id = null, $user_id = null)
    {
        $user_id = $user_id != null ? $user_id : user()->id;
        return \Corefinex\Message\App\Model\UserHasMessage::query()
                ->where('user_id', $user_id)
                ->when($message_id != null, function ($q) use ($message_id) {
                    return $q->where('id', $message_id);
                })
                ->whereNull('seen_at')
                ->get() ?? null;
    }
}
if (!function_exists('get_messages')) {
    function get_messages($id = null)
    {
        return \Corefinex\Message\App\Model\Message::query()
            ->when($id != null, function ($q) use ($id) {
                return $q->where('id', $id);
            })->get();
    }
}
