<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->integer('sender_id');
            $table->string('title');
            $table->text('description');
            $table->enum('level', ['low', 'normal', 'high'])->default('normal');
            $table->enum('type', ['public', 'private'])->default('public');
            $table->timestamps();
        });
        Schema::create('user_has_message', function (Blueprint $table) {
            $table->integer('message_id');
            $table->integer('user_id');
            $table->timestamp('seen_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('user_has_message');
    }
}
